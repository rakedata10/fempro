#ifndef NODES_H
#define NODES_H

#include <gmCoreModule>
#include <gmTriangleSystemModule>

class Nodes
{
public:
    Nodes();
    Nodes(GMlib::TSVertex<float>& pt);
    void setZ(float d);
    bool isThis(GMlib::TSVertex<float>* _pt);

    GMlib::TSVertex<float>* getVertex();
    GMlib::TSEdge<float>* neighbour(Nodes& n);
    GMlib::Array<GMlib::TSTriangle<float> *> getTriangles();
    GMlib::Array<GMlib::TSEdge<float>* > getEdges();
    GMlib::TSEdge<float>* edge;
private:
    GMlib::TSVertex<float>* _pt;
};

#endif // NODES_H
