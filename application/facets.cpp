#include "facets.h"
#include "nodes.h"
#include <QDebug>
#include <gmCoreModule>
#include <gmSceneModule>

Facets::Facets(int n, double radius): GMlib::TriangleFacets<float>()
{
    _numbOfBoundaryNodes = 0;
    _maxInterval = 4;
    randomnodes(n,radius);
    _func = 0;
    _down = true;
}

Facets::Facets(int n,int m, double radius): GMlib::TriangleFacets<float>()
{
    _numbOfBoundaryNodes = 0;
    makeNodes(n, m, radius);
    _maxInterval = 4;
    _func = 0;
    _down = true;
}
//Facets::Facets(int n, double radius)
Facets::Facets(GMlib::ArrayLX<GMlib::TSVertex<float>> & pt)

{
    _numbOfBoundaryNodes = 0;

}

void Facets::makeNodes(int n, int m, double radius)
{


    GMlib::ArrayLX<GMlib::TSVertex<float>> nodes;
    int k=1;
    int size = 1+n*(m*(m+1))/2;
    std::cout << size << std::endl;
    nodes.setSize(size);
    nodes[0].setPos( GMlib::Point<float,2>(0,0));

    for(int j = 0; j < m; j++)
    {
        for(int i = 0; i < n*(j+1); i++)
        {
            GMlib::Angle a = GMlib::Angle(i * (M_2PI)/(n*(j+1)));
            GMlib::SqMatrix<float,2> mt(a, GMlib::Vector<float,2>(1,0), GMlib::Vector<float,2>(0,1));
            GMlib::Vector<float,2> p(((j+1)*radius)/m,0);
            //qDebug() << "Position: " << mt;
            nodes[k++].setPos(mt*p);
        }
    }
    _numbOfBoundaryNodes = n*m;
    insertAlways(nodes);

}
bool Facets::pointok(const GMlib::Point<float,2>& pt, float min_dist){

    for(int i=0; i < this->getSize(); i++)
    {
        float length = (pt-(*this)[i].getParameter()).getLength();
        qDebug() << length << " : " << min_dist;
        if ( length < min_dist)
            return false;
    }
    return true;
}

void Facets::randomnodes(int k, float radius )
{
    float area = M_PI *radius*radius; //area of the large circle
    float b = ( M_2PI * radius) / k; //length of one side of the triang
    float c=  std::sqrt(3)/4 * b * b; //area of the traing

    int t = int (area/c) ;//no of traingles;



    float inner_pt = 1 + int(float(t-k)/2 + 0.5);  //inner point
    float radius_inner=std::sqrt(radius * radius - (b/2)*(b/2));
    float min_dist=std::sqrt(area/(std::sqrt(3)*t))*2/1.5;

    GMlib::ArrayLX<GMlib::TSVertex<float> > nodes;
    nodes.setSize(k + inner_pt);
    GMlib::Vector<float,2> p(radius,0);

    //qDebug() << area << a << c << t;

    for(int i = 0; i < k; i++)
    {
        GMlib::Angle a = GMlib::Angle(i * M_2PI/k);
        GMlib::SqMatrix<float,2> mt(a, GMlib::Vector<float,2>(1,0), GMlib::Vector<float,2>(0,1));
        nodes[i].setPos(mt*p);
    }


    GMlib::Random<float> rand(-radius, radius);

    for(int j = 0; j < inner_pt; j++)
    {
        GMlib::Point<float,2> pt;
        do{
            do{
                pt=GMlib::Point<float,2>(rand.get(),rand.get());
            }while(pt.getLength()>radius_inner);
        }while(!pointok(pt,min_dist));
        nodes[k+j].setPos(pt);

    }
    this->insertAlways(nodes);
}


//computing stiffness matrix and load vector



void Facets::computeValue()
{

    //Create nodes, removing the bounding vertices

    for(int i = 0; i < this->size(); i++)
    {
        if(!(*this)[i].boundary())
            nodes += Nodes((*this)[i]);
    }
}
void Facets::stiffness()
{
    //Create stiffness matrix
    _A.setDim(nodes.size(), nodes.size());

    for(int i = 0; i < nodes.size(); i++)
        for(int j = 0; j < nodes.size(); j++)
            _A[i][j] = 0;


    //    qDebug() << "nodesize: " << nodes.getSize();

    for (int i=0; i < nodes.size() ;i++)
    {
        for(int j = i+1; j < nodes.size(); j++)
        {
            GMlib::TSEdge<float> * edge = nodes[i].neighbour(nodes[j]);
            if(edge != nullptr)
            {



                GMlib::Vector<GMlib::Vector<float,2>,3> d = calculateVectors(edge);

                float d0d0 = d[0] * d[0];
                //                qDebug() << "d0d0: " << d0d0 << " at: " << i << ":" << j;
                float dd = 1 / (d0d0);  //properties of diagonal

                //for horizontal length

                float dh1 = dd * (d[1] * d[0]);
                float dh2 = dd * (d[2] * d[0]);

                //calculation of area
                float area1 = std::abs(d[0] ^ d[1]);
                float area2 = std::abs(d[2] ^ d[0]);

                //for verticalheight

                float h1 = dd * area1 * area1;
                float h2 = dd * area2 * area2;


                // qDebug() << h1  << ":" << h2 << ":" << dh1 << ":" << dh2;


                //computing stiffness matrix outside the diagonal

                _A[i][j]=_A[j][i]= (((dh1 *(1-dh1))/h1)-dd) * (area1/2) + (((dh2 *(1-dh2))/h2)-dd) * (area2/2);

                qDebug() << "Aij:" << _A[i][j];

            }

            //            std::cout << "Matrix A: " << _A << std::endl;
        }

        //computing stiffness matrix diagonal elements

        //        qDebug() << "finished a, now diagonal";


        GMlib::Array<GMlib::TSTriangle<float>* > triangular = nodes[i].getTriangles();

        for (int j=0; j < triangular.getSize(); j++)
        {
            GMlib::Vector<GMlib::Vector<float,2>,3>  d = calculateVectors(nodes[i], triangular[j]);
            _A[i][i] += (d[2]*d[2]) / (2*std::abs(d[0] ^ d[1]));

        }

    }
    //Create the load vector

    _b.setDim(nodes.size());
    for(int i = 0; i < nodes.size(); i++){
        GMlib::Array<GMlib::TSTriangle<float>* > triangle = nodes[i].getTriangles();
        //
        _b[i] = triangle[0]->getArea2D()/3;

        for(int j = 1; j < triangle.size(); j++)
            _b[i] += triangle[j]->getArea2D()/3;
    }

    qDebug() << "Vector b: " << _b;
    //Invert the stiffness matrix


    _Ainvert = _A.invert();



}


GMlib::Vector<GMlib::Vector<float,2>,3> Facets::calculateVectors(Nodes pn, GMlib::TSTriangle<float>* triangle)
{
    GMlib::Array<GMlib::TSVertex<float>* > vertices=triangle->getVertices();
    GMlib::Vector<GMlib::Vector<float,2>,3> d;

    if(vertices[1] == pn.getVertex())
    {
        std::swap( vertices[1], vertices[0]);
        std::swap( vertices[1], vertices[2]);
    }
    else if(vertices[2] == pn.getVertex())
    {
        std::swap(vertices[0],vertices[2]);
        std::swap(vertices[2],vertices[1]);
    }

    GMlib::Point<float,2> p0=vertices[0]->getParameter();
    GMlib::Point<float,2> p1=vertices[1]->getParameter();
    GMlib::Point<float,2> p2=vertices[2]->getParameter();

    d[0]=p1-p0;
    d[1]=p2-p0;
    d[2]=p2-p1;

    return d;
}


GMlib::Vector<GMlib::Vector<float,2>,3> Facets::calculateVectors(GMlib::TSEdge<float>* edge)
{
    GMlib::Vector<GMlib::Vector<float,2>,3> d;


    GMlib::Array<GMlib::TSTriangle<float>*> triangle = edge->getTriangle();
    GMlib::Array<GMlib::TSVertex<float>*> t1 = triangle[0]->getVertices();
    GMlib::Array<GMlib::TSVertex<float>*> t2 = triangle[1]->getVertices();


    GMlib::Point<float,2> p0=edge->getFirstVertex()->getParameter();
    GMlib::Point<float,2> p1=edge->getLastVertex()->getParameter();
    GMlib::Point<float,2> p2,p3;

    for (int i=0; i<3; i++)
    {

        if((t1[i] != edge->getFirstVertex()) && (t1[i]!=edge->getLastVertex()))
            p2 = t1[i]->getParameter();

        if((t2[i] != edge->getFirstVertex()) && (t2[i] != edge->getLastVertex()))
            p3 = t2[i]->getParameter();
    }

    //Vector Definitions(2D)

    d[0] = p1 - p0;
    d[1] = p2 - p0;
    d[2] = p3 - p0;
    //qDebug() << "Vector d: " << d;
    return d;

    //std::cout << "value d: " << d << std::endl;
}

void Facets::hgtupdate(float a)
{

    GMlib::DVector<float> b = a * _b;
    GMlib::DVector<float> x = _Ainvert * b;
    x.setDim(_b.getDim());
    for(int i = 0; i < nodes.size(); i++)
    {
        nodes[i].setZ(x[i]);
    }
    //        std::cout << "value b: " << b << std::endl;
}


void Facets::simuLation()
{
    start=true;
}

void Facets::localSimulate(double dt)
{
    if (start){

        if(_down)
            _func += dt*3;
        else
            _func -= dt*3;

        // qDebug() << _func << " " << _down;

        if(_func > _maxInterval)
            _down = false;

        if(_func < -_maxInterval)
            _down = true;

        this->hgtupdate(_func);
        this->replot();

    }
}




