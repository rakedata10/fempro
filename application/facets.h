#include<gmCoreModule>
#include <gmTriangleSystemModule>
#include <QtCore>
#include <gmSceneModule>
#include"nodes.h"

class Facets:public GMlib::TriangleFacets<float>
{
public:
    Facets(int n, int m, double radius);
    Facets(int k, double radius);
    Facets( GMlib::ArrayLX<GMlib::TSVertex<float>> & pt);
    void    stiffness();
    void    simuLation();
    void    makeNodes(int n, int m, double radius);
    void    computeValue();
    void    makeLoadVector();
    void    makerandomNodes();
    void    hgtupdate(float a);
    void    randomnodes(int n, float radius );
    int     getsize();
    int     numberOfBoundaryNodes() const;
    bool    start;
    bool    pointok(const GMlib::Point<float,2>& pt, float min_dist);
    GMlib::Vector<GMlib::Vector<float,2>,3> calculateVectors(GMlib::TSEdge<float>* edge);
    GMlib::Vector<GMlib::Vector<float,2>,3> calculateVectors(Nodes pn,GMlib::TSTriangle<float>* triangle);

private:

    GMlib::ArrayLX<Nodes> nodes;
   // to solve Ax=b
    GMlib::DMatrix<float> _A;
    GMlib::DMatrix<float> _Ainvert;
    GMlib::DVector<float> _b;

    bool                _down;
    bool                _start;
    float               _func;
    int                 _numbOfBoundaryNodes;
    float _maxInterval;
    float _update;

protected:
void localSimulate(double dt);



};
